/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "myvector.h"
#include <stdlib.h>

vector_t*
new_vector(size_t type_size, size_t num_elements)
{
  vector_t* v = malloc(sizeof(vector_t) + (type_size * num_elements));
  if (v == NULL) {
    perror("Error on creating vector.\n");
    return NULL;
  } else {
    v->items = malloc(type_size + num_elements);
    if (v->items == NULL) {
      perror("Erorr on allocating items for vector.\n");
      free(v);
      return NULL;
    }
    v->size = 0;
    v->capacity = num_elements;
    return v;
  }
}

void
vector_delete(vector_t* v)
{
  free(v->items);
  free(v);
}

void
vector_push(vector_t* v, void* ele)
{
  size_t new_size = v->size + 1; // since we add only 1 element at a time.
  if (new_size >= v->capacity) {
    vector_reszie(v, 2*v->capacity);
  }
  v->items[new_size] = ele;
  v->size++;
}

void
vector_resize(vector_t* v, size_t new_size)
{
  v->items = realloc(v, new_size);
  if (v->items == NULL) {
    perror("Can't resize vector.\n");
  }
  v->capacity = new_size;
}



// void
// new_vector_error(void* item)
// {
// perror("Error on creating vector.\n");
// }

// vector_t*
// new_vector_item(void* items)
// {
// TODO size_of_each_item is wrong
// size_t size_of_struct = sizeof(vector_t);
// size_t size_of_each_item = sizeof(items)/sizeof(items[0]);
// vector_t* new_vec = malloc(size_of_struct + size_of_each_item +
// _MYC_DEFAULT_VEC_CAP_); if (new_vec == NULL) { perror("Error on creating
// vector.\n"); return NULL;
// }
// new_vec->size = size_of_each_item;
// new_vec->capacity = _MYC_DEFAULT_VEC_CAP_;
// new_vec->items = malloc(size_of_each_item * sizeof(items));
// if (new_vec->items == NULL) {
// perror("Error on creating vector.\n");
// free(new_vec);
// return NULL;
// }
// for (int i = 0; i <=)

// }
