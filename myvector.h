/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stdio.h>

#define _MYC_DEFAULT_VEC_CAP_ 1024

// #define new_vector(X)                                                   \
  // _Generic((x),                                                         \
           // size_t: new_vector_cap,                                      \
           // void**: new_vector_items,                                    \
           // default: new_vector_error)

typedef struct
{
  void** items;
  size_t size;     // no. of current items
  size_t capacity; // maximum no. of items
} vector_t;

// create vector with a given array
// vector_t*
// new_vector_items(void** items);

// create vector with a given capacity
// vector_t*
// new_vector_cap(size_t cap);

// create error on creation
// void
// new_vector_error(void* item);

vector_t*
new_vector(size_t type_size, size_t num_elements);

void
vector_delete(vector_t* v);

void
vector_push(vector_t* v, void* ele);

// change capacity of a vector v to new_size.
void
vector_reszie(vector_t* v, size_t new_size);
