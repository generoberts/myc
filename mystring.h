/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stdbool.h>
#include <stdio.h>

#define _MYC_DEFAULT_STR_CAP_ 1024

#define string_concat(old, new)                                                \
  _Generic((old),                                                       \
           string_t*: _Generic((new),                                   \
                               string_t*: string_concats,               \
                               const char*: string_concatcc,            \
                               char*: string_concatc,                   \
                               default:  string_concat_error),          \
           default: _Generic((new),                                     \
                             default: string_concat_error))

typedef struct
{
  char* string;
  size_t length; // not include the null terminator
  size_t capacity;
} string_t;

string_t*
new_string(const char* init);

void
string_concatcc(string_t* old_str, const char* append);

void
string_concatc(string_t* old_str, char* append);

void
string_concats(string_t* old_str, string_t* append);

// used to indicate error in genric selection
// TODO will this work? since it's used in a generic
void
string_concat_error(void);

size_t
string_length(string_t* str);

bool
string_empty(string_t* str);

void
string_reverse(string_t* str);

string_t*
string_find(string_t* str, const char* pattern);

void
string_free(string_t* str);
