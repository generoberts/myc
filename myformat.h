/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// because I'm not gonna remember all these

#pragma once

#define PRINT_INT "%d"
#define PRINT_UINT "%u"
#define PRINT_FLOAT "%f"
#define PRINT_DOUBLE "%f"
#define PRINT_SCIENCE_LOWER "%e"
#define PRINT_SCIENCE_UPPER "%E"
#define PRINT_CHAR "%c"
#define PRINT_STRING "%s"
#define PRINT_POINTER "%p"
