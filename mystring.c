/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mystring.h"
#include <error.h>
#include <stdlib.h>
#include <string.h>

static size_t
double_capacity(string_t* str)
{
  size_t result = str->capacity;
  return 2 * result;
}

string_t*
new_string(const char* init)
{
  size_t struct_size = sizeof(string_t);
  string_t* str;
  size_t string_size = strlen(init) + 1;

  if (string_size > _MYC_DEFAULT_STR_CAP_) {
    perror("String too long.\n");
    return NULL;
  }

  str = malloc(_MYC_DEFAULT_STR_CAP_ + struct_size);
  if (str == NULL) {
    perror("Can't init string_t.\n");
    return NULL;
  }

  str->string = malloc(_MYC_DEFAULT_STR_CAP_);
  if (str->string == NULL) {
    perror("Can't init string.\n");
    free(str);
    return NULL;
  }

  snprintf(str->string, string_size, "%s", init);
  str->length = strlen(str->string);
  str->capacity = _MYC_DEFAULT_STR_CAP_;

  return str;
}

void
string_concatcc(string_t* old_str, const char* append)
{
  size_t totol_size = string_length(old_str) + strlen(append) + 1;
  if (totol_size > old_str->capacity) {
    char* tmp = realloc(old_str->string, double_capacity(old_str));
    if (tmp != NULL) {
      old_str->string = tmp;
    } else {
      perror("Realloc failed at string_concatcc.\n");
      string_free(old_str);
    }
    old_str->capacity = double_capacity(old_str);
  }
  snprintf(old_str->string, totol_size, "%s%s", old_str->string, append);
}

void
string_concatc(string_t* old_str, char* append)
{
  string_concatcc(old_str, append);
}

void
string_concats(string_t* old_str, string_t* append)
{
  string_concatcc(old_str, append->string);
}

// used to indicate error in genric selection
void
string_concat_error(void)
{
  perror("Can't cancate string.\n");
  exit(EXIT_FAILURE);
}

size_t
string_length(string_t* str)
{
  return str->length;
}

bool
string_empty(string_t* str)
{
  return str->length == 0 ? true : false;
}

void
string_reverse(string_t* str)
{
  char* tmp = malloc(strlen(str->string) + 1);
  if (tmp == NULL) {
    perror("Malloc fail at string_reverse.\n");
    return;
  }
  size_t tpm_length = str->length;
  // TODO check
  for (int i = str->length; i > 0; --i) {
    tmp[tpm_length - i] = str->string[i];
  }
  str->string = tmp;
  free(tmp);
}

string_t*
string_find(string_t* src, const char* pattern)
{
  string_t* result = new_string("");
  char* str = src->string;
  char* sub_string = strstr(str, pattern);
  if (sub_string == NULL) { // src->string doesn't contain pattern
    return NULL;
  } else {
    result = (string_t*)string_concat(result->string, sub_string);
    return result;
  }
}

void
string_free(string_t* str)
{
  free(str->string);
  free(str);
}
