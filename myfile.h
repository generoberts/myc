/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#pragma once

#include <stdio.h>
#include <stdbool.h>

#include "mystring.h"

typedef FILE file;

file*
open_file(string_t* file_name, const char* mode);

int
close_file(file* f);

bool
is_file_exists(string_t* file_name);

void
copy_file(string_t* from, string_t* to);
