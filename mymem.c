/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mymem.h"
#include <stdio.h>

void*
sallocv(size_t size, const char* message)
{
  void* result = malloc(size);
  if (result == NULL) {
    printf("%s\n", message);
    return NULL;
  }
  return result;
}

void*
salloc(size_t size)
{
  void* result = malloc(size);
  if (result == NULL) {
    perror("Can't allocate memory in salloc.\n");
    return NULL;
  }
  return result;
}
