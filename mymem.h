/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
  Memory safer of malloc
*/

#pragma once

#include <stdlib.h>

// v at the end stands for verbose
void*
sallocv(size_t size, const char* message);

void*
salloc(size_t size);
