/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <unistd.h>

#include "myfile.h"
#include "mystring.h"

file*
open_file(string_t* file_name, const char* mode)
{
  file* openned = fopen(file_name->string, mode);
  if (openned != NULL) {
    return openned;
  } else {
    perror("Error open file.\n");
    return NULL;
  }
}

int
close_file(file* f)
{
  int status = fclose(f);
  if (status != 0) {
    perror("Error closing file.\n");
    return status;
  } else {
    return status;
  }
}

bool
is_file_exists(string_t* file_name)
{
  if (access(file_name->string, F_OK) != -1) {
    return true;
  } else {
    return false;
  }
}

void
copy_file(string_t* from, string_t* to)
{
  int c;
  file* read = open_file(from, "r");
  file* write = open_file(to, "w");

  while ((c = fgetc(read)) != EOF) {
    fputc(c, write);
  }

  close_file(read);
  close_file(write);
}
